import { StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native'
import React from 'react'
import { Animasi } from "../../assets";
import { WARNA_BODY } from '../../utils/constant';
import Font from '../../assets/fonts/Font';

const Akun = () => {
  return (
    <View style={styles.page}>
      <View>
        <Text style={styles.navbar}>Akun</Text>
      </View>
      <View style={styles.gambar}>
        <Animasi/>
      </View>
      <View style={styles.deskripsi}>
        <Text>Upss kamu belum memiliki akun. Mulai buat akun</Text>
        <Text>agar transaksi di BCR lebih mudah</Text>
      </View>
      <View style={styles.button}>
          <TouchableOpacity>
            <Text style={styles.buttonKeterangan}>Register</Text>
          </TouchableOpacity>
      </View>
    </View>
  )
}

export default Akun

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: WARNA_BODY
  },
  navbar: {
    fontSize: 18,
    paddingLeft: 30,
    paddingTop: 15,
    color: 'black',
    fontFamily: Font.PoppinsBold
  },
  gambar: {
    marginTop: 100,
    marginLeft: 45
  },
  deskripsi: {
    alignItems: 'center',
    marginLeft: 10
  },
  button: {
    backgroundColor: '#5CB85F',
    borderRadius: 5,
    width: '25%',
    height: 40,
    alignItems: 'center',
    marginTop: 20,
    marginLeft: 150
  },
  buttonKeterangan: {
    marginTop: 10,
    color: 'white'
  }
})