import { StyleSheet, Text, View, ImageBackground, Dimensions, Image } from 'react-native'
import React from 'react'
import { ImageHeader, ProvileImg } from "../../assets";
import { ScrollView, TouchableHighlight } from 'react-native-gesture-handler';
import Font from '../../assets/fonts/Font';
import { ButtonIcon, Iklan, ListMobil } from '../../components';
import { WARNA_BODY } from '../../utils/constant';

const Home = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsHorizontalScrollIndicator={false}>
        <ImageBackground source={ImageHeader} style={styles.header}>
          <View style={styles.headerText}>
            <Text style={styles.nama}>Hi, Muhammad Hadyan Qodri</Text>
            <Text style={styles.lokasi}>Palembang</Text>
          </View>
          <TouchableHighlight style={[styles.profileImgContainer, { borderColor: 'black', borderWidth: 0.7 }]}>
            <Image source={ProvileImg} style={styles.ProvileImg} ></Image>
          </TouchableHighlight>
        </ImageBackground>
        <Iklan />
        <View style={styles.layanan}>
          <ButtonIcon title="Sewa Mobil" />
          <ButtonIcon title="Oleh-Oleh" />
          <ButtonIcon title="Penginapan" />
          <ButtonIcon title="Wisata" />
        </View>
        <View style={styles.listMobil}>
          <Text style={styles.label}>Daftar Mobil Pilihan</Text>
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
          <ListMobil />
        </View>
      </ScrollView>
    </View>
  )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: WARNA_BODY
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.24,
    flexDirection: 'row'
  },
  ProvileImg: {
    height: windowHeight * 0.07,
    width: windowWidth * 0.14,
    borderRadius: 40,
  },
  profileImgContainer: {
    marginLeft: 85,
    marginVertical: windowHeight * 0.040,
    height: windowHeight * 0.072,
    width: windowWidth * 0.143,
    borderRadius: 40,
  },
  headerText: {
    marginLeft: 30,
    marginVertical: windowHeight * 0.05
  },
  nama: {
    fontSize: 14,
    color: 'black'
  },
  lokasi: {
    fontSize: 18,
    fontFamily: Font.PoppinsBold,
    color: 'black'
  },
  layanan: {
    paddingHorizontal: 30,
    paddingTop: 25,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  listMobil: {
    paddingHorizontal: 30,
    paddingTop: 25,
  },
  label: {
    fontSize: 18,
    color: 'black',
    fontFamily: Font.PoppinsBold
  }
})