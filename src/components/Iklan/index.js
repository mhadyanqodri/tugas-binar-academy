import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import Font from '../../assets/fonts/Font';
import { IklanImage } from '../../assets';

const Iklan = () => {
  return (
    <View style={styles.container}>
      <View style={styles.keteranganIklan}>
        <Text style={styles.keterangan}>
          Sewa Mobil Berkualitas
          di kawasanmu
        </Text>
        <View style={styles.buttonKeterangan}>
          <TouchableOpacity>
            <Text>Sewa Mobil</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Image source={IklanImage} style={styles.IklanImage}></Image>
    </View>
  )
}

export default Iklan;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#091B6F',
    padding: 24,
    marginHorizontal: 30,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.30,
    shadowRadius: 4.65,
    elevation: 8,
    marginTop: -windowHeight * 0.10,
    flexDirection: 'row',
  },
  keteranganIklan: {
    width: '55%',
  },
  keterangan: {
    color: 'white',
    marginBottom: 15
  },
  buttonKeterangan: {
    backgroundColor: '#5CB85F',
    borderRadius: 5,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    width: '60%'
  },
  IklanImage: {
    marginLeft: -26,
    marginBottom: -windowHeight
  }
})