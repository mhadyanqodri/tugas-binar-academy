import SplashBackground from "./SplashBackground.png";
import ImageHeader from "./header.png";
import Banner from "./Banner.png";
import ProvileImg from "./ProvileImg.png";
import IklanImage from "./IklanImage.png";
import Mobil from "./Mobil.png";

export { SplashBackground, ImageHeader, Banner, ProvileImg, IklanImage, Mobil }